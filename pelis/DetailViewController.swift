//
//  DetailViewController.swift
//  pelis
//
//  Created by Antonio Pertusa on 14/12/16.
//  Copyright © 2016 Antonio Pertusa. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {


    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!

    func configureView() {
        // Update the user interface for the detail item.
        if let peli = self.detailItem {

            if let title = peli.title {
                self.navigationItem.title = "\(title))"
                if let year = peli.year {
                    self.navigationItem.title =  "\(title) (\(year))"
                }
            }
            
            self.textView?.text = peli.synopsis
            if let image = peli.image {
                self.imageView?.image = image
            } else {
                self.imageView?.image = UIImage(named:"Placeholder.png")
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async{ // Para hacer que el textView se visualize desde la primera línea
            self.textView.scrollRangeToVisible(NSMakeRange(0, 0))
        }
    }

    var detailItem: Pelicula? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }


}

